exports.proj = {
  Title: 'Basic web site Frame'
};

exports.root = '/Presentation/';
exports.rootPath = exports.root+'assets/';
exports.viewFolder = exports.root+'Pages/';
exports.allViewFolder = exports.root+'Pages/views/';
exports.virtualRootPath = '/virt/';

exports.controllerFolder = './controller/';
exports.jsonPath = "/src/Config/Routes.json";
exports.ServerPort = 8090;

exports.errorController = './controller/error';
